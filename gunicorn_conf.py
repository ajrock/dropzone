import os

_port = os.getenv("API_PORT", "8000")

bind = f"0.0.0.0:{_port}"
