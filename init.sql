CREATE TABLE dropzone (
  id SERIAL NOT NULL PRIMARY KEY,
  time_stamp timestamp with time zone,
  mean NUMERIC,
  standard_deviation NUMERIC
);