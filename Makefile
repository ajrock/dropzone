install:
	pipenv install --dev

teardown:
	docker-compose down --remove-orphans --volumes --timeout=5

bootstrap: teardown start-api
	docker-compose pull
	docker-compose run wait-for-dependencies
	docker-compose run init-s3

e2e-no-bootstrap:
	PIPENV_DOTENV_LOCATION="bootstrap.env" pipenv run pytest tests/e2e -vv

e2e: bootstrap e2e-no-bootstrap
	docker-compose down --remove-orphans --volumes --timeout=5

start-api:
	docker-compose up --build --detach api
	@echo "API running on http://localhost:8000"

start-api-local:
	PIPENV_DOTENV_LOCATION="bootstrap.env" pipenv run uvicorn --reload src.main:app
