import os


def parse_users():
    """
    in the ENV we expect a string like
        "user:password,new_user:new_password"
    :return: dict with usernames as keys and passwords as values
    """

    env_content = os.environ["AUTH_USERS"]

    return dict([i.split(":") for i in env_content.split(",")])


AUTH_USERS = parse_users()
PUBLIC_BASE_URL = os.environ.get("PUBLIC_BASE_URL", "http://localhost:8000")

S3_ENDPOINT = os.environ.get("S3_ENDPOINT", "https://s3.eu-central-1.amazonaws.com")
S3_BUCKET_NAME = os.environ.get("S3_BUCKET_NAME", "dropzone")

DB_HOST = os.environ["DB_HOST"]
DB_PORT = os.environ["DB_PORT"]
DB_USER = os.environ["DB_USER"]
DB_PASSWORD = os.environ["DB_PASSWORD"]
DB_NAME = os.environ["DB_NAME"]
TABLE_NAME = os.environ["TABLE_NAME"]

SQLALCHEMY_DATABASE_URI = f"postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}"
