from sqlalchemy import Column, Integer, DateTime, Float
from sqlalchemy.ext.declarative import declarative_base

from config import TABLE_NAME


Base = declarative_base()


class Readings(Base):
    """
    Data as stored in database.
    Pay attention to the transformations applied as they do not map 1-1 with input models.
    """
    __tablename__ = TABLE_NAME
    id = Column(Integer, primary_key=True, index=True)
    time_stamp = Column(DateTime)
    mean = Column(Float)
    standard_deviation = Column(Float)
