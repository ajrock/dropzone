from typing import List

from pydantic import BaseModel


class JsonDataBase(BaseModel):
    time_stamp: str
    data: List[float]


class JsonData(JsonDataBase):
    mean: float
    standard_deviation: float

    class Config:
        orm_mode = True
