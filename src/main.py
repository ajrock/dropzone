from fastapi import FastAPI

from api.post import router
from storage.utils import connect_to_storage


app = FastAPI(title="Ginkgo Data Dropzone API")
app.add_event_handler("startup", connect_to_storage)
app.include_router(router)


@app.get("/")
def read_root():
    return {"Hello": "World"}
