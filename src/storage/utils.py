from storage.db import PG

"""
This allows storage media swapping more easily, by abstracting one layer from the DB itself.
"""


class DataBase:
    storage: PG


db = DataBase()


async def get_storage() -> PG:
    return db.storage


async def connect_to_storage():
    connection = PG()
    db.storage = connection
    return
