import json

import boto3

from config import S3_ENDPOINT, S3_BUCKET_NAME
from models.schemas import JsonDataBase


def store_raw_data_to_s3(data: JsonDataBase, received_at):
    s3_client = boto3.client("s3", endpoint_url=S3_ENDPOINT)

    s3_client.put_object(
        Body=json.dumps(data.dict()).encode("utf8"),
        Bucket=S3_BUCKET_NAME,
        Key=f"received_at={received_at}.json"
    )
