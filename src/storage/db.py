from dateutil.parser import parse

from pytz import timezone
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from config import SQLALCHEMY_DATABASE_URI
from models.db_models import Readings


class PG:
    """
    Basic DB class to maintain connectivity with postgres.
    """
    def __init__(self):
        self._setup_db()

    def _setup_db(self):
        engine = create_engine(SQLALCHEMY_DATABASE_URI)
        session = sessionmaker(autoflush=False, bind=engine)
        self._db = session()

    @staticmethod
    def calculate_statistics(data):
        mean = sum(data) / len(data)
        deviation = [abs(item - mean) ** 2 for item in data]
        standard_deviation = (sum(deviation) / len(deviation)) ** 0.5

        return mean, standard_deviation

    @staticmethod
    def convert_timezone(time_stamp):
        dt = parse(time_stamp)
        return dt.astimezone(timezone("UTC"))

    def store_data(self, json_data):
        mean, standard_deviation = self.calculate_statistics(json_data.data)
        time_stamp = self.convert_timezone(json_data.time_stamp)
        db_data = Readings(time_stamp=time_stamp, mean=mean, standard_deviation=standard_deviation)
        self._db.add(db_data)
        self._db.commit()

    def get_all_data(self):
        return self._db.query(Readings).all()
