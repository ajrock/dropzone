import secrets

from fastapi import HTTPException, status, Depends
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from config import AUTH_USERS


security = HTTPBasic()


def authenticate(credentials: HTTPBasicCredentials = Depends(security)) -> str:
    """
    FastAPI-provided authentication. Checks credentials supplied by
    user against AUTH_USER set and raises 401 if not authorized.
    Is wrapped by Depends in endpoints.
    """

    # Could hash passwords as well: compare_digest prevents timing attacks
    if credentials.username in AUTH_USERS and secrets.compare_digest(
        credentials.password, AUTH_USERS[credentials.username]
    ):
        return credentials.username

    raise HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Token is invalid",
        headers={"WWW-Authenticate": "Basic"},
    )
