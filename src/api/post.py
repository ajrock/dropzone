from datetime import datetime

from fastapi import APIRouter, Depends

from api.authentication import authenticate
from models.schemas import JsonDataBase
from storage.db import PG
from storage.utils import get_storage
from storage.s3_storage import store_raw_data_to_s3

router = APIRouter()


@router.post("/v1/create")
def receive_json_file(
    *,
    storage: PG = Depends(get_storage),
    data: JsonDataBase,
    is_user_valid: bool = Depends(authenticate)
):
    """
    Create new data record from JSON. Parsable time_stamp (as string) required or throws 500 error.
    Data is a list of numeric values (anything else causes 500 error.
    """
    received_at = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
    store_raw_data_to_s3(data, received_at)
    storage.store_data(data)
    return {"status": "OK"}


@router.get("/v1/read")
def read_json_file(
    *,
    storage: PG = Depends(get_storage),
    is_user_valid: bool = Depends(authenticate)
):
    """
    Check all existing records. TO BE DONE: filter files on timestamp.
    """
    return storage.get_all_data()
