import requests

from config import PUBLIC_BASE_URL


def test_no_access_unauthenticated():
    response = requests.get(PUBLIC_BASE_URL + "/v1/read", auth=("wrong", "credentials"))
    assert response.status_code == 401


def test_access_authenticated():
    response = requests.get(PUBLIC_BASE_URL + "/v1/read", auth=("u", "p"))
    assert response.status_code == 200
