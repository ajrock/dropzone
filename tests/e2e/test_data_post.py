import json

import boto3
import requests

from config import PUBLIC_BASE_URL, S3_BUCKET_NAME

S3_ENDPOINT = "http://localhost:4566"


def test_post_data():
    with open('test.json', 'r') as f:
        data = json.load(f)

    post_response = requests.post(PUBLIC_BASE_URL + "/v1/create", data=json.dumps(data), auth=("u", "p"))

    assert post_response.status_code == 200

    read_response = requests.get(PUBLIC_BASE_URL + "/v1/read", auth=("u", "p"))
    assert read_response.status_code == 200

    expected = {
        "standard_deviation": 1.0128088525931498,
        "id": 1,
        "time_stamp": "2019-05-01T06:00:00+00:00",
        "mean": 0.9985282517566028
        }

    received = json.loads(read_response.text)[0]

    assert expected['standard_deviation'] == received['standard_deviation']
    assert expected['mean'] == received['mean']
    assert expected['time_stamp'] == received['time_stamp']
    assert 1 == received["id"]

    s3_client = boto3.resource("s3", endpoint_url=S3_ENDPOINT)
    bucket = s3_client.Bucket(S3_BUCKET_NAME)
    bucket_len = len([_ for _ in bucket.objects.filter(Prefix="rec")])
    assert bucket_len > 0
