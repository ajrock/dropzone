FROM python:3.9-slim

RUN pip install gunicorn

RUN groupadd -g 999 appuser && useradd -r -u 999 -g appuser appuser

WORKDIR /home/appuser/app

RUN pip install pipenv
COPY Pipfile Pipfile.lock /home/appuser/app/
RUN pipenv install --system --deploy

COPY src /home/appuser/app/src/

RUN chown -R appuser /home/appuser/app/
USER appuser

ENV PYTHONPATH="./src"

COPY ./gunicorn_conf.py /gunicorn_conf.py
CMD ["gunicorn", "--config=/gunicorn_conf.py", "-k","uvicorn.workers.UvicornWorker","src.main:app"]